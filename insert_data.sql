insert into roles(name, description, timestamp) values
('ROLE_ADMIN', 'admin', current_timestamp),
('ROLE_USER', 'user', current_timestamp);

insert into dorms(name, numberofrooms, timestamp) values
('Leu A', 140, current_timestamp),
('Leu C', 120, current_timestamp),
('P3', 100, current_timestamp),
('P14', 50, current_timestamp);

insert into rooms(iddorm, idroom, capacity, remainingslots, nottaken, status, timestamp) values
(1, 230, 3, 3, 3, True, current_timestamp),
(1, 522, 3, 3, 3, True, current_timestamp),
(1, 704, 2, 2, 2, True, current_timestamp),
(1, 1002, 3, 3, 3, True, current_timestamp),
(2, 101, 2, 2, 2, True, current_timestamp),
(2, 110, 2, 2, 2, True, current_timestamp),
(2, 207, 2, 2, 2, True, current_timestamp),
(2, 312, 2, 2, 2, True, current_timestamp),
(3, 401, 4, 4, 4, True, current_timestamp),
(3, 402, 4, 4, 4, True, current_timestamp),
(3, 403, 4, 4, 4, True, current_timestamp),
(3, 404, 4, 4, 4, True, current_timestamp),
(4, 8, 4, 4, 4, True, current_timestamp),
(4, 9, 4, 4, 4, True, current_timestamp),
(4, 10, 4, 4, 4, True, current_timestamp),
(4, 503, 4, 4, 4, True, current_timestamp);

insert into yearsofstudy(name, priority, timestamp) values
('I', 4, current_timestamp),
('II', 3, current_timestamp),
('III', 2, current_timestamp),
('IV', 1, current_timestamp);

insert into accounts(username, password, idrole, attempts, active, resetattemptsdate, unlockaccountdate, timestamp) values
('admin', 'admin', 1, 0, true, current_timestamp, current_timestamp, current_timestamp),
('user', 'user', 2, 0, true, current_timestamp, current_timestamp, current_timestamp),
('user2', 'user2', 2, 0, true, current_timestamp, current_timestamp, current_timestamp),
('lstanciu', 'lstanciu', 2, 0, true, current_timestamp, current_timestamp, current_timestamp),
('gpascu', 'gpascu', 2, 0, true, current_timestamp, current_timestamp, current_timestamp),
('aveselu', 'aveselu', 2, 0, true, current_timestamp, current_timestamp, current_timestamp);

insert into students(idaccount, lastname, firstname, cnp, email, dateofbirth, idyearofstudy, grade, failures, roomrequest, timestamp) values
('4', 'Stanciu', 'Laurentiu', '1900110100100', 'laurentiustanciu@yahoo.com', current_timestamp, 4, 10.0, 0, true, current_timestamp),
('5', 'Pascu', 'Gabriel', '1900210100101', 'gabrielpascu@yahoo.com', current_timestamp, 4, 10.0, 0, true, current_timestamp),
('6', 'Veselu', 'Adrian', '1900310100102', 'adrianveselu@yahoo.com', current_timestamp, 4, 10.0, 0, true, current_timestamp);

