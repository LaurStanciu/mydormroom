create table if not exists Dorms(
  idDorm smallserial primary key,
  Name text,
  NumberOfRooms smallint,
  Timestamp timestamp,
  unique(Name)
);

create table if not exists Rooms(
  IdDormRoom smallserial primary key,
  IdDorm smallint,
  IdRoom smallint,
  Capacity numeric(1,0),
  RemainingSlots numeric(1,0),
  NotTaken numeric(1,0),
  Status bool,
  Timestamp timestamp,
  unique(IdDorm, IdRoom),
  foreign key (IdDorm) references Dorms (IdDorm) on update cascade on delete cascade
);

create table if not exists YearsOfStudy(
  IdYearOfStudy smallserial primary key,
  Name text,
  Priority smallint,
  Timestamp timestamp,
  unique(Name, Priority)
);

create table if not exists Students(
  IdStudent serial primary key,
  IdAccount int,
  LastName text,
  FirstName text,
  CNP varchar(13),
  Email text,
  DateOfBirth date,
  IdYearOfStudy smallint,
  Grade decimal(4,2),
  Failures smallint,
  RoomRequest bool,
  Timestamp timestamp,
  unique(IdAccount),
  unique(cnp),
  unique (Email),
  foreign key (IdYearOfStudy) references YearsOfStudy (IdYearOfStudy) on update cascade on delete cascade,
  foreign key (IdAccount) references Accounts (IdAccount) on update cascade on delete cascade
);

create table if not exists StudentsRoomsAffiliation(
  IdStudentRoomAffi serial primary key,
  IdDormRoom smallint,
  IdStudent int,
  SetInDate timestamp,
  unique(IdStudent),
  foreign key (IdDormRoom) references Rooms (IdDormRoom) on update cascade on delete cascade,
  foreign key (IdStudent) references Students (IdStudent) on update cascade on delete cascade
);

create table if not exists RoomRequests(
  IdRoomRequest serial primary key,
  IdStudent int,
  IdDormRoom smallint,
  Timestamp timestamp,
  unique(IdStudent),
  foreign key (IdStudent) references Students (IdStudent) on update cascade on delete cascade
);

create table if not exists RoomRequestsAdditionalInformation(
  IdRoomRequestInfo serial primary key,
  IdStudent int,
  IdColleague int,
  Status bool default false,
  Timestamp timestamp,
  unique(IdStudent, IdColleague),
  foreign key (IdStudent) references RoomRequests (IdStudent) on update cascade on delete cascade,
  foreign key (IdColleague) references Students (IdStudent) on update cascade on delete cascade
);

create table if not exists ChangeRoomRequests(
  IdChangeRoomRequest serial primary key,
  IdStudentSource int,
  IdStudentDestination int,
  Status bool default false,
  Timeout timestamp,
  Timestamp timestamp,
  foreign key (IdStudentSource) references Students (IdStudent) on update cascade on delete cascade,
  foreign key (IdStudentDestination) references Students (IdStudent) on update cascade on delete cascade
);

create table if not exists Roles(
  IdRole smallserial primary key,
  Name text,
  Description text,
  Timestamp timestamp,
  unique(Name)
);

create table if not exists Accounts(
  IdAccount smallserial primary key,
  Username text,
  Password text,
  IdRole int,
  Attempts numeric(1,0),
  Active boolean default false,
  Resetattemptsdate timestamp,
  Unlockaccountdate timestamp,
  Timestamp timestamp,
  unique(Username),
  foreign key (IdRole) references Roles (IdRole) on update cascade on delete cascade
);

create table if not exists BlockAnyChanges(
  IdBlockAnyChange serial primary key,
  IdInitiator int,
  StartingDate timestamp,
  FinishingDate timestamp,
  Active boolean default false,
  TImestamp timestamp,
  foreign key (IdInitiator) references Accounts (IdAccount) on update cascade on delete cascade
);

