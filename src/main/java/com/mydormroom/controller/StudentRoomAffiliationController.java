package com.mydormroom.controller;

import com.google.common.collect.Lists;
import com.mydormroom.model.StudentRoomAffiliation;
import com.mydormroom.repo.StudentRoomAffiliationRepository;
import com.mydormroom.utilities.ResponseMessage;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by Laur Stanciu on 2/17/2018.
 */
@RestController
@RequestMapping("/api/studentsroomsaffiliation")
public class StudentRoomAffiliationController {

    private StudentRoomAffiliationRepository studentRoomAffiliationRepository;

    public StudentRoomAffiliationController(StudentRoomAffiliationRepository studentRoomAffiliationRepository) {
        this.studentRoomAffiliationRepository = studentRoomAffiliationRepository;
    }

    @RequestMapping(value = "/savecurrentroom", method = RequestMethod.POST, produces = "application/json")
    public ResponseMessage saveCurrentRoom(@RequestBody StudentRoomAffiliation studentRoomAffiliation) {
        try {
            studentRoomAffiliationRepository.save(studentRoomAffiliation);
            return new ResponseMessage("Registered!", 0, HttpServletResponse.SC_OK, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseMessage(ex.toString(), 0, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/findbyiddormroomcurrentroom/{iddormroom}", method = RequestMethod.GET)
    public List<StudentRoomAffiliation> findByDormRoomCurrentRoom(@PathVariable("iddormroom") int idDormRoom) {
        return Lists.newArrayList(studentRoomAffiliationRepository.findByIdDormRoom(idDormRoom));
    }

    @RequestMapping(value = "/findbyidstudentcurrentroom/{idstudent}", method = RequestMethod.GET)
    public List<StudentRoomAffiliation> findByStudentCurrentRoom(@PathVariable("idstudent") int idStudent) {
        return Lists.newArrayList(studentRoomAffiliationRepository.findByIdStudent(idStudent));
    }
}
