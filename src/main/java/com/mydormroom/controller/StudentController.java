package com.mydormroom.controller;

import com.google.common.collect.Lists;
import com.mydormroom.model.Student;
import com.mydormroom.repo.StudentRepository;
import com.mydormroom.utilities.ResponseMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by Laur Stanciu on 2/17/2018.
 */
@RestController
@RequestMapping("/api/students")
public class StudentController {

    private StudentRepository studentRepository;

    public StudentController(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
    public ResponseMessage save(@RequestBody Student student){
        try {
            studentRepository.save(student);
            return new ResponseMessage("Registered!", 0, HttpServletResponse.SC_OK, HttpStatus.OK);
        }
        catch (Exception ex){
            return new ResponseMessage(ex.toString(), 0, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/findbyidstudent/{idstudent}", method = RequestMethod.GET)
    public Student findByIdStudent(@PathVariable("idstudent") int idStudent){
        return studentRepository.findByIdStudent(idStudent);
    }

    @RequestMapping(value = "/findbyemail/{email}", method = RequestMethod.GET)
    public Student findByEmail(@PathVariable("email") String email){
        return studentRepository.findByEmail(email);
    }

    @RequestMapping(value = "/findbylastnamefirstname/{lastname}/{firstname}", method = RequestMethod.GET)
    public List<Student> findByLastNameFirstName(@PathVariable("lastname") String lastName, @PathVariable("firstname") String firstName){
        return Lists.newArrayList(studentRepository.findByLastNameContainsAndFirstNameContains(lastName, firstName));
    }

    @RequestMapping(value = "/findbyyearofstudy/{yearofstudy}", method = RequestMethod.GET)
    public List<Student> findByYearOfStudy(@PathVariable("yearofstudy") int yearOfStudy){
        return Lists.newArrayList(studentRepository.findByIdYearOfStudy(yearOfStudy));
    }

    @RequestMapping(value = "/findall", method = RequestMethod.GET)
    public List<Student> findAll(){
        return Lists.newArrayList(studentRepository.findAll());
    }
}
