package com.mydormroom.controller;

import com.google.common.collect.Lists;
import com.mydormroom.model.Dorm;
import com.mydormroom.repo.DormRepository;
import com.mydormroom.utilities.ResponseMessage;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by Laur on 2/3/2018.
 */

@RestController
@RequestMapping("/api/dorms")
public class DormController {

    private DormRepository dormRepository;

    public DormController(DormRepository dormRepository){
        this.dormRepository = dormRepository;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
    public ResponseMessage process(@RequestBody Dorm dorm){
        try {
            dormRepository.save(dorm);
            return new ResponseMessage("Registered!", 0, HttpServletResponse.SC_OK, HttpStatus.OK);
        }
        catch (Exception ex){
            return new ResponseMessage(ex.toString(), 0, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/findbyname/{name}", method = RequestMethod.GET)
    public List<Dorm> findByName(@PathVariable("name") String name) {
        return Lists.newArrayList(dormRepository.findByNameContains(name));
    }

    @RequestMapping(value = "/findall", method = RequestMethod.GET)
    public List<Dorm> findAll(){
        return Lists.newArrayList(dormRepository.findAll());
    }

}
