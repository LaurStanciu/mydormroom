package com.mydormroom.controller;

import com.google.common.collect.Lists;
import com.mydormroom.model.Account;
import com.mydormroom.model.Role;
import com.mydormroom.repo.AccountRepository;
import com.mydormroom.repo.RoleRepository;
import com.mydormroom.utilities.ResponseMessage;
import org.apache.catalina.connector.Response;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static com.mydormroom.utilities.Constants.USE_BCRYPT;

/**
 * Created by Laur on 2/14/2018.
 */

@RestController
@PreAuthorize("hasAuthority('ROLE_ADMIN')")
@RequestMapping("api/accounts")
public class AccountController {

    private AccountRepository accountRepository;
    private RoleRepository roleRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public AccountController(AccountRepository accountRepository,
                             RoleRepository roleRepository,
                             BCryptPasswordEncoder bCryptPasswordEncoder){
        this.accountRepository = accountRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
    public ResponseMessage save(@RequestBody Account account){
        try {
            if(USE_BCRYPT)
                account.setPassword(bCryptPasswordEncoder.encode(account.getPassword()));
            accountRepository.save(account);
            return new ResponseMessage("Registered!", 0, HttpServletResponse.SC_OK, HttpStatus.OK);
        }
        catch(Exception ex){
            return new ResponseMessage("Invalid request: " + ex.toString(), 0,
                    HttpServletResponse.SC_INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/findbyusername/{username}", method = RequestMethod.GET)
    public Account findByUsername(@PathVariable("username") String username){
        return accountRepository.findByUsername(username);
    }

    @RequestMapping(value = "/findall", method = RequestMethod.GET)
    public List<Account> findAll(){
        return Lists.newArrayList(accountRepository.findAll());
    }

    @RequestMapping(value = "/saverole", method = RequestMethod.POST, produces = "application/json")
    public ResponseMessage save(@RequestBody Role role){
        try{
            roleRepository.save(role);
            return new ResponseMessage("Registered!", 0, HttpServletResponse.SC_OK,
                    HttpStatus.OK);
        }catch(Exception ex){
            return new ResponseMessage(ex.toString(), 0, HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/findallroles", method = RequestMethod.GET)
    public List<Role> findAllRoles(){
        return Lists.newArrayList(roleRepository.findAll());
    }

}
