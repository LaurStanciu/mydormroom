package com.mydormroom.controller;

import com.google.common.collect.Lists;
import com.mydormroom.dto.RoomRequestAdditionalInformationDto;
import com.mydormroom.dto.RoomRequestCompleteDto;
import com.mydormroom.model.*;
import com.mydormroom.repo.*;
import com.mydormroom.utilities.ResponseMessage;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Laur Stanciu on 2/17/2018.
 */
@RestController
@RequestMapping("/api/roomrequests")
public class RoomRequestController {

    private RoomRepository roomRepository;
    private StudentRepository studentRepository;
    private RoomRequestRepository roomRequestRepository;
    private RoomRequestAdditionalInformationRepository roomRequestAdditionalInformationRepository;
    private StudentRoomAffiliationRepository studentRoomAffiliationRepository;
    private BlockAnyChangeRepository blockAnyChangeRepository;

    public RoomRequestController(RoomRepository roomRepository, StudentRepository studentRepository,
                                 RoomRequestRepository roomRequestRepository,
                                 RoomRequestAdditionalInformationRepository roomRequestAdditionalInformationRepository,
                                 StudentRoomAffiliationRepository studentRoomAffiliationRepository, BlockAnyChangeRepository blockAnyChangeRepository) {
        this.roomRepository = roomRepository;
        this.studentRepository = studentRepository;
        this.roomRequestRepository = roomRequestRepository;
        this.roomRequestAdditionalInformationRepository = roomRequestAdditionalInformationRepository;
        this.studentRoomAffiliationRepository = studentRoomAffiliationRepository;
        this.blockAnyChangeRepository = blockAnyChangeRepository;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
    public ResponseMessage save(@RequestBody RoomRequestCompleteDto roomRequestCompleteDto) {
        try {
            String message = "";
            int extraData = 0;
            boolean badRequest = false;
            BlockAnyChange restriction;
            Room requestedRoom;
            StudentRoomAffiliation studentRoomAffiliation;
            RoomRequest roomRequest;


            restriction = blockAnyChangeRepository.findBlockAnyChangeByStartingDateIsLessThanEqualAndFinishingDateGreaterThanEqual(new Date(), new Date());

            if(restriction != null){
                return new ResponseMessage("Requests are restricted until " + restriction.getFinishingDate() + "!", 0, HttpServletResponse.SC_BAD_REQUEST, HttpStatus.BAD_REQUEST);
            }

            requestedRoom = roomRepository.findByIdDormRoom(roomRequestCompleteDto.getIdDormRoom().getIdDormRoom());
            studentRoomAffiliation = studentRoomAffiliationRepository.findByIdStudent(roomRequestCompleteDto.getIdStudent());

            if(roomRequestAdditionalInformationRepository.findByIdStudent(roomRequestCompleteDto.getIdStudent()).size() != 0)
                return new ResponseMessage("You already have active invites!", 0, HttpServletResponse.SC_BAD_REQUEST, HttpStatus.BAD_REQUEST);

            if(studentRoomAffiliation != null) {
                return new ResponseMessage("You already have a room!", 0, HttpServletResponse.SC_BAD_REQUEST, HttpStatus.BAD_REQUEST);
            }
            if(requestedRoom.getRemainingSlots() == 0) {
                return new ResponseMessage("This room is full!", 0, HttpServletResponse.SC_BAD_REQUEST, HttpStatus.BAD_REQUEST);
            }
            if(requestedRoom.getRemainingSlots() - 1 < roomRequestCompleteDto.getColleagues().size()) {
                return new ResponseMessage("Too many colleagues added!", 0, HttpServletResponse.SC_BAD_REQUEST, HttpStatus.BAD_REQUEST);
            }
            roomRequest = new RoomRequest(roomRequestCompleteDto.getIdRoomRequest(), roomRequestCompleteDto.getIdStudent(), roomRequestCompleteDto.getIdDormRoom());

            if(roomRequestAdditionalInformationRepository.findByIdStudent(roomRequestCompleteDto.getIdStudent()) != null)
                roomRequestAdditionalInformationRepository.deleteRoomRequestAdditionalInformationByIdStudent(roomRequestCompleteDto.getIdStudent());
            if (roomRequestCompleteDto.getColleagues() != null) {
                for (String colleagueEmail : roomRequestCompleteDto.getColleagues()) {
                    Student colleague = studentRepository.findByEmail(colleagueEmail);
                    if (colleague == null || colleague.getIdStudent() == roomRequestCompleteDto.getIdStudent()) {
                        message += " " + colleagueEmail;
                        badRequest = true;
                    }
                    if (!badRequest) {
                        if(studentRoomAffiliationRepository.findByIdStudent(colleague.getIdStudent()) != null) {
                            message = " " + colleagueEmail + " already has a room";
                            badRequest = true;
                        }
                    }
                }
                if (!badRequest) {
                    roomRequestRepository.save(roomRequest);
                    for (String colleagueEmail : roomRequestCompleteDto.getColleagues()) {
                        Student colleague = studentRepository.findByEmail(colleagueEmail);
                        roomRequestAdditionalInformationRepository.save(new RoomRequestAdditionalInformation(roomRequestCompleteDto.getIdStudent(),colleague.getIdStudent()));
                        requestedRoom.setRemainingSlots(requestedRoom.getRemainingSlots() - 1);
                    }
                } else return new ResponseMessage("Invalid request:" + message + "!", extraData, HttpServletResponse.SC_BAD_REQUEST, HttpStatus.OK);
            } else {
                roomRequestRepository.save(roomRequest);
            }

            requestedRoom.setRemainingSlots(requestedRoom.getRemainingSlots() - 1);
            requestedRoom.setNotTaken(requestedRoom.getNotTaken() - 1);

            roomRepository.save(requestedRoom);

            if (!badRequest) {
                studentRoomAffiliationRepository.save(new StudentRoomAffiliation(roomRequest.getIdDormRoom().getIdDormRoom(), roomRequest.getIdStudent()));
            }

            roomRequest = roomRequestRepository.findByIdStudent(roomRequestCompleteDto.getIdStudent());
            extraData = roomRequest.getIdRoomRequest();

            if (roomRequestCompleteDto.getIdRoomRequest() == 0)
                return new ResponseMessage("Room Request created!", extraData, HttpServletResponse.SC_OK, HttpStatus.OK);
            else return new ResponseMessage("Room Request changed!", extraData, HttpServletResponse.SC_OK, HttpStatus.OK);

        } catch (Exception ex) {
            return new ResponseMessage(ex.toString(), 0, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/acceptroomrequest", method = RequestMethod.POST, produces = "application/json")
    public ResponseMessage acceptRoomRequest(@RequestBody RoomRequestAdditionalInformationDto roomRequestAdditionalInformationDto){
        RoomRequestAdditionalInformation roomRequestAdditionalInformation = roomRequestAdditionalInformationRepository.findByIdRoomRequestInfo(roomRequestAdditionalInformationDto.getIdRoomRequestInfo());
        RoomRequest roomRequest = roomRequestRepository.findByIdStudent(roomRequestAdditionalInformation.getIdStudent());
        Room room = roomRepository.findByIdDormRoom(roomRequest.getIdDormRoom().getIdDormRoom());

        room.setNotTaken(room.getNotTaken() - 1);
        studentRoomAffiliationRepository.save(new StudentRoomAffiliation(roomRequest.getIdDormRoom().getIdDormRoom(), roomRequestAdditionalInformation.getIdColleague()));
        roomRepository.save(room);
        roomRequestAdditionalInformationRepository.deleteRoomRequestAdditionalInformationByIdColleague(roomRequestAdditionalInformation.getIdColleague());
        return new ResponseMessage("Request accepted!", 0, HttpServletResponse.SC_OK, HttpStatus.OK);
    }

    @RequestMapping(value = "/removeroomrequest", method = RequestMethod.POST, produces = "application/json")
    public ResponseMessage removeRoomRequest(@RequestBody RoomRequestAdditionalInformationDto roomRequestAdditionalInformationDto){
        RoomRequestAdditionalInformation roomRequestAdditionalInformation = roomRequestAdditionalInformationRepository.findByIdRoomRequestInfo(roomRequestAdditionalInformationDto.getIdRoomRequestInfo());
        RoomRequest roomRequest = roomRequestRepository.findByIdStudent(roomRequestAdditionalInformation.getIdStudent());


        Room requestedRoom = roomRepository.findByIdDormRoom(roomRequest.getIdDormRoom().getIdDormRoom());
        requestedRoom.setRemainingSlots(requestedRoom.getRemainingSlots() + 1);
        roomRepository.save(requestedRoom);
        roomRequestAdditionalInformationRepository.deleteRoomRequestAdditionalInformationByIdRoomRequestInfo(roomRequestAdditionalInformationDto.getIdRoomRequestInfo());
        return new ResponseMessage("Request removed!", 0, HttpServletResponse.SC_OK, HttpStatus.OK);
    }

    @RequestMapping(value = "/findbyiddormroom/{iddormroom}", method = RequestMethod.GET)
    public List<RoomRequest> findByIdDormRoom(@PathVariable("iddormroom") Room idDormRoom) {
        return Lists.newArrayList(roomRequestRepository.findByIdDormRoom(idDormRoom));
    }

    @RequestMapping(value = "/findfullroomrequestbyidstudent/{idstudent}", method = RequestMethod.GET)
    public RoomRequestCompleteDto findFullRoomRequestByIdStudent (@PathVariable("idstudent") int idStudent) {
        RoomRequest roomRequest = roomRequestRepository.findByIdStudent(idStudent);
        if (roomRequest != null) {
            List<RoomRequestAdditionalInformation> additionalInfo = roomRequestAdditionalInformationRepository.findByIdStudent(idStudent);
            List<String> colleagues = new ArrayList<String>();
            List<Integer> colleaguesIds = additionalInfo.stream().map(RoomRequestAdditionalInformation::getIdColleague).collect(Collectors.toList());
            for (int idColleague : colleaguesIds) {
                colleagues.add(studentRepository.findByIdStudent(idColleague).getEmail());
            }
            return new RoomRequestCompleteDto(roomRequest.getIdRoomRequest(), idStudent, roomRequest.getIdDormRoom(), colleagues);
        }
        else return new RoomRequestCompleteDto();
    }

    @RequestMapping(value = "/findbyidstudent/{idstudent}", method = RequestMethod.GET)
    public List<RoomRequest> findByIdStudent(@PathVariable("idstudent") int idStudent) {
        return Lists.newArrayList(roomRequestRepository.findByIdStudent(idStudent));
    }

    @RequestMapping(value = "/findall", method = RequestMethod.GET)
    public List<RoomRequest> findAll() {
        return Lists.newArrayList(roomRequestRepository.findAll());
    }

    @RequestMapping(value = "/saveadditionalinformation", method = RequestMethod.POST, produces = "application/json")
    public ResponseMessage save(@RequestBody RoomRequestAdditionalInformation roomRequestAdditionalInformation) {
        try {
            roomRequestAdditionalInformationRepository.save(roomRequestAdditionalInformation);
            return new ResponseMessage("Registered!", 0, HttpServletResponse.SC_OK, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseMessage(ex.toString(), 0, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/findbyadditionalinformationidroomrequest/{idstudent}", method = RequestMethod.GET)
    public List<RoomRequestAdditionalInformation> findByAdditionalIdStudent(@PathVariable("idstudent") int idStudent) {
        return Lists.newArrayList(roomRequestAdditionalInformationRepository.findByIdStudent(idStudent));
    }
}
