package com.mydormroom.controller;

import com.mydormroom.dto.ChangeRoomRequestResponseDto;
import com.mydormroom.dto.NotificationsDto;
import com.mydormroom.dto.RoomRequestAdditionalInformationDto;
import com.mydormroom.model.ChangeRoomRequest;
import com.mydormroom.model.RoomRequestAdditionalInformation;
import com.mydormroom.model.StudentRoomAffiliation;
import com.mydormroom.repo.*;
import com.mydormroom.security.ITokenHelper;
import io.jsonwebtoken.Claims;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Laur Stanciu on 4/16/2018.
 */

@RestController
@RequestMapping("api/utility")

public class UtilityController {

    private ITokenHelper tokenHelper;
    private RoomRequestAdditionalInformationRepository roomRequestAdditionalInformationRepository;
    private ChangeRoomRequestRepository changeRoomRequestRepository;
    private BlockAnyChangeRepository blockAnyChangeRepository;
    private StudentRepository studentRepository;
    private StudentRoomAffiliationRepository studentRoomAffiliationRepository;

    public UtilityController(ITokenHelper tokenHelper,
                             RoomRequestAdditionalInformationRepository roomRequestAdditionalInformationRepository,
                             ChangeRoomRequestRepository changeRoomRequestRepository,
                             BlockAnyChangeRepository blockAnyChangeRepository,
                             StudentRepository studentRepository,
                             StudentRoomAffiliationRepository studentRoomAffiliationRepository){
        this.tokenHelper = tokenHelper;
        this.roomRequestAdditionalInformationRepository = roomRequestAdditionalInformationRepository;
        this.changeRoomRequestRepository = changeRoomRequestRepository;
        this.blockAnyChangeRepository = blockAnyChangeRepository;
        this.studentRepository = studentRepository;
        this.studentRoomAffiliationRepository = studentRoomAffiliationRepository;
    }

    @RequestMapping(value = "/checkupdates", method = RequestMethod.GET)
    public NotificationsDto checkToken(HttpServletRequest request, HttpServletResponse response){
        Date currentDate = new Date();
        NotificationsDto notificationsDto = new NotificationsDto();
        List<RoomRequestAdditionalInformationDto> roomRequestAdditionalInformationDtos = new ArrayList<>();
        List<ChangeRoomRequestResponseDto> changeRoomRequestResponseDtos = new ArrayList<>();
        List<RoomRequestAdditionalInformation> roomRequestAdditionalInformations = new ArrayList<RoomRequestAdditionalInformation>();
        List<ChangeRoomRequest> changeRoomRequests = new ArrayList<ChangeRoomRequest>();
        boolean blockChanges = false;
        boolean hasRoom = false;

        Claims claims = tokenHelper.parseToken(request);
        if(claims.get("studentIdentification") != null) {
            roomRequestAdditionalInformations = roomRequestAdditionalInformationRepository.findByIdColleagueAndStatus(((int) claims.get("studentIdentification")), false);
            if(roomRequestAdditionalInformations.size() != 0){
                roomRequestAdditionalInformationDtos = roomRequestAdditionalInformations.stream().
                        map(roomRequestAdditionalInformation -> new RoomRequestAdditionalInformationDto(roomRequestAdditionalInformation.getIdRoomRequestInfo(),
                                studentRepository.findByIdStudent(roomRequestAdditionalInformation.getIdStudent()).getEmail()))
                        .collect(Collectors.toList());
            }
            changeRoomRequests = changeRoomRequestRepository.findByIdStudentDestinationAndTimeoutGreaterThan((int) claims.get("studentIdentification"), currentDate);
            if(changeRoomRequests.size() != 0){
                changeRoomRequestResponseDtos = changeRoomRequests.stream()
                        .map(changeRoomRequest -> new ChangeRoomRequestResponseDto(changeRoomRequest.getIdChangeRoomRequest(), studentRepository.findByIdStudent(changeRoomRequest.getIdStudentSource()).getEmail()))
                        .collect(Collectors.toList());
            }
            blockChanges = blockAnyChangeRepository.countBlockAnyChangeByStartingDateIsLessThanEqualAndFinishingDateGreaterThanEqual(currentDate, currentDate) != 0 ? true : false;
            hasRoom = studentRoomAffiliationRepository.findByIdStudent((int) claims.get("studentIdentification")) != null ? true : false;
        }
        notificationsDto = new NotificationsDto(changeRoomRequestResponseDtos.size() + roomRequestAdditionalInformationDtos.size(), roomRequestAdditionalInformationDtos, changeRoomRequestResponseDtos, blockChanges, hasRoom);
        return notificationsDto;
    }
}
