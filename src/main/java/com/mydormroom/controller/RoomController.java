package com.mydormroom.controller;

import com.google.common.collect.Lists;
import com.mydormroom.dto.RoomDto;
import com.mydormroom.model.Dorm;
import com.mydormroom.model.Room;
import com.mydormroom.model.StudentRoomAffiliation;
import com.mydormroom.repo.DormRepository;
import com.mydormroom.repo.RoomRepository;
import com.mydormroom.repo.StudentRepository;
import com.mydormroom.repo.StudentRoomAffiliationRepository;
import com.mydormroom.utilities.ResponseMessage;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Laur on 2/12/2018.
 */

@RestController
@RequestMapping("/api/rooms")
public class RoomController {

    private RoomRepository roomRepository;
    private DormRepository dormRepository;
    private StudentRoomAffiliationRepository studentRoomAffiliationRepository;
    private StudentRepository studentRepository;

    public RoomController(RoomRepository roomRepository, DormRepository dormRepository, StudentRoomAffiliationRepository studentRoomAffiliationRepository, StudentRepository studentRepository){
        this.roomRepository = roomRepository;
        this.dormRepository = dormRepository;
        this.studentRoomAffiliationRepository = studentRoomAffiliationRepository;
        this.studentRepository = studentRepository;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
    public ResponseMessage save(@RequestBody Room room){
        try {
            roomRepository.save(room);
            return new ResponseMessage("Registered!", 0, HttpServletResponse.SC_OK, HttpStatus.OK);
        }
        catch (Exception ex){
            return new ResponseMessage(ex.toString(), 0, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/findbyroom/{iddorm}/{idroom}", method = RequestMethod.GET)
    public Room findByCaminAndCamera(@PathVariable("iddorm") int IdDorm, @PathVariable("idroom") int IdRoom){
        return roomRepository.findByIdDormAndIdRoom(IdDorm, IdRoom);
    }

    @RequestMapping(value = "/myroom/{idStudent}", method = RequestMethod.GET)
    public RoomDto findMyRoom(@PathVariable("idStudent") int idStudent){
        try {
            StudentRoomAffiliation studentRoomAffiliation = studentRoomAffiliationRepository.findByIdStudent(idStudent);
            List<StudentRoomAffiliation> studentRoomAffiliations = studentRoomAffiliationRepository.findByIdDormRoom(studentRoomAffiliation.getIdDormRoom());
            Room room = roomRepository.findByIdDormRoom(studentRoomAffiliation.getIdDormRoom());
            Dorm dorm = dormRepository.findOne(room.getIdDorm());
            List<String> colleagues = new ArrayList<>();

            for (StudentRoomAffiliation studentRoomAffili : studentRoomAffiliations) {
                if (studentRoomAffili.getIdStudent() != idStudent) {
                    colleagues.add(studentRepository.findByIdStudent(studentRoomAffili.getIdStudent()).getEmail());
                }
            }

            return new RoomDto(dorm.getName(), room.getIdRoom(), room.getRemainingSlots(), colleagues);
        }
        catch (Exception ex){
            return new RoomDto();
        }
    }

    @RequestMapping(value = "/findbydorm/{iddorm}", method = RequestMethod.GET)
    public List<Room> findByIdDorm(@PathVariable("iddorm") int IdDorm){
        return roomRepository.findByIdDormOrderByIdRoom((IdDorm));
    }

    @RequestMapping(value = "/findavailablebydorm/{iddorm}", method = RequestMethod.GET)
    public List<Room> findAvailableByIdDorm(@PathVariable("iddorm") int IdDorm){
        return roomRepository.findByIdDormAndRemainingSlotsGreaterThan(IdDorm, 0);
    }

    @RequestMapping(value = "/findavailable", method = RequestMethod.GET)
    public List<Room> findAvailable(){
        return Lists.newArrayList(roomRepository.findByRemainingSlotsGreaterThan(0));
    }

    @RequestMapping(value = "/findall", method = RequestMethod.GET)
    public List<Room> findAll(){
        return Lists.newArrayList(roomRepository.findAll());
    }
}
