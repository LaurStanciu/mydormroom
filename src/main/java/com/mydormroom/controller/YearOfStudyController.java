package com.mydormroom.controller;

import com.google.common.collect.Lists;
import com.mydormroom.model.YearOfStudy;
import com.mydormroom.repo.YearOfStudyRepository;
import com.mydormroom.security.ITokenHelper;
import com.mydormroom.utilities.ResponseMessage;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static com.mydormroom.utilities.SecurityConstants.HEADER_STRING;
import static com.mydormroom.utilities.SecurityConstants.TOKEN_PREFIX;

/**
 * Created by Laur Stanciu on 2/16/2018.
 */
@RestController
@RequestMapping("api/yearsofstudy")
public class YearOfStudyController {

    private YearOfStudyRepository yearOfStudyRepository;

    public YearOfStudyController(YearOfStudyRepository yearOfStudyRepository){
        this.yearOfStudyRepository = yearOfStudyRepository;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
    public ResponseMessage save(@RequestBody YearOfStudy yearOfStudy){
        try {
            yearOfStudyRepository.save(yearOfStudy);
            return new ResponseMessage("Registered!", 0, HttpServletResponse.SC_OK, HttpStatus.OK);
        }catch (Exception ex){
            return new ResponseMessage(ex.toString(), 0, HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/findallorderdesc", method = RequestMethod.GET)
    public List<YearOfStudy> findAllOrderDesc(){
        return Lists.newArrayList(yearOfStudyRepository.findAllByOrderByPriorityDesc());
    }

    @RequestMapping(value = "/findall", method = RequestMethod.GET)
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public List<YearOfStudy> findAll(){
        return Lists.newArrayList(yearOfStudyRepository.findAll());
    }
}
