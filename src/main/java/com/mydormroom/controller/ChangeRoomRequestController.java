package com.mydormroom.controller;

import com.google.common.collect.Lists;
import com.mydormroom.dto.ChangeRoomRequestCreateDto;
import com.mydormroom.dto.ChangeRoomRequestResponseDto;
import com.mydormroom.model.BlockAnyChange;
import com.mydormroom.model.ChangeRoomRequest;
import com.mydormroom.model.Student;
import com.mydormroom.model.StudentRoomAffiliation;
import com.mydormroom.repo.BlockAnyChangeRepository;
import com.mydormroom.repo.ChangeRoomRequestRepository;
import com.mydormroom.repo.StudentRepository;
import com.mydormroom.repo.StudentRoomAffiliationRepository;
import com.mydormroom.utilities.ResponseMessage;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;

/**
 * Created by Laur Stanciu on 2/17/2018.
 */
@RestController
@RequestMapping("/api/changeroomrequests")
public class ChangeRoomRequestController {

    private ChangeRoomRequestRepository changeRoomRequestRepository;
    private StudentRepository studentRepository;
    private StudentRoomAffiliationRepository studentRoomAffiliationRepository;
    private BlockAnyChangeRepository blockAnyChangeRepository;

    public ChangeRoomRequestController(ChangeRoomRequestRepository changeRoomRequestRepository, StudentRepository studentRepository, StudentRoomAffiliationRepository studentRoomAffiliationRepository, BlockAnyChangeRepository blockAnyChangeRepository){
        this.changeRoomRequestRepository = changeRoomRequestRepository;
        this.studentRepository = studentRepository;
        this.studentRoomAffiliationRepository = studentRoomAffiliationRepository;
        this.blockAnyChangeRepository = blockAnyChangeRepository;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = "application/json")
    public ResponseMessage create(@RequestBody ChangeRoomRequestCreateDto changeRoomRequestCreateDto){
        int extraData;
        Student receiver;
        StudentRoomAffiliation receiverRoom;
        StudentRoomAffiliation senderRoom;
        List<ChangeRoomRequest> senderChangeRoomRequests;
        BlockAnyChange restriction;

        restriction = blockAnyChangeRepository.findBlockAnyChangeByStartingDateIsLessThanEqualAndFinishingDateGreaterThanEqual(new Date(), new Date());

        if(restriction != null){
            return new ResponseMessage("Requests are restricted until " + restriction.getFinishingDate() + "!", 0, HttpServletResponse.SC_BAD_REQUEST, HttpStatus.BAD_REQUEST);
        }

        receiver = studentRepository.findByEmail(changeRoomRequestCreateDto.getReceiverEmail());
        receiverRoom = studentRoomAffiliationRepository.findByIdStudent(receiver.getIdStudent());

        if(receiver.getIdStudent() == changeRoomRequestCreateDto.getSenderIdStudent()){
            return new ResponseMessage("You cannot switch rooms with yourself!", 0, HttpServletResponse.SC_BAD_REQUEST, HttpStatus.BAD_REQUEST);
        }

        if(receiverRoom == null)
            return new ResponseMessage("Receiver does not have an assigned room!", 0, HttpServletResponse.SC_BAD_REQUEST, HttpStatus.OK);

        senderRoom = studentRoomAffiliationRepository.findByIdStudent(changeRoomRequestCreateDto.getSenderIdStudent());
        if(senderRoom == null)
            return new ResponseMessage("You do not have an assigned room!", 0, HttpServletResponse.SC_BAD_REQUEST, HttpStatus.OK);

        if(receiverRoom.getIdDormRoom() == senderRoom.getIdDormRoom())
            return new ResponseMessage("You cannot switch rooms with roommates!", 0, HttpServletResponse.SC_BAD_REQUEST, HttpStatus.OK);

        ChangeRoomRequest receiverChangeRoomRequests = changeRoomRequestRepository.findByIdStudentSourceAndTimeoutGreaterThan(receiver.getIdStudent(), new Date());

        if(receiverChangeRoomRequests != null)
            return new ResponseMessage("Receiver has an active change room request!", 0, HttpServletResponse.SC_BAD_REQUEST, HttpStatus.OK);

        senderChangeRoomRequests = changeRoomRequestRepository.findByIdStudentDestinationAndTimeoutGreaterThan(senderRoom.getIdStudent(), new Date());

        if(senderChangeRoomRequests.size() != 0)
            return new ResponseMessage("You have an active change room requests from other students!", 0, HttpServletResponse.SC_BAD_REQUEST, HttpStatus.OK);

        changeRoomRequestRepository.deleteChangeRoomRequestByIdStudentSource(changeRoomRequestCreateDto.getSenderIdStudent());
        changeRoomRequestRepository.save(new ChangeRoomRequest(changeRoomRequestCreateDto.getSenderIdStudent(), receiver.getIdStudent(), false));
        extraData = changeRoomRequestRepository.findByIdStudentSourceAndTimeoutGreaterThan(changeRoomRequestCreateDto.getSenderIdStudent(), new Date()).getIdChangeRoomRequest();

        return new ResponseMessage("Change Room Request created!", extraData, HttpServletResponse.SC_OK, HttpStatus.OK);
    }

    @RequestMapping(value = "/accept", method = RequestMethod.POST, produces = "application/json")
    public ResponseMessage accept(@RequestBody ChangeRoomRequestResponseDto changeRoomRequestResponseDto){
        int auxRoom;
        ChangeRoomRequest changeRoomRequest = changeRoomRequestRepository.findOne(changeRoomRequestResponseDto.getIdChangeRoomRequest());
        StudentRoomAffiliation receiverRoom = studentRoomAffiliationRepository.findByIdStudent(changeRoomRequest.getIdStudentDestination());
        StudentRoomAffiliation senderRoom = studentRoomAffiliationRepository.findByIdStudent(changeRoomRequest.getIdStudentSource());

        if(senderRoom == null)
            return new ResponseMessage("You do not have an assigned room!", 0, HttpServletResponse.SC_BAD_REQUEST, HttpStatus.OK);

        if(receiverRoom == null)
            return new ResponseMessage("Receiver does not have an assigned room!", 0, HttpServletResponse.SC_BAD_REQUEST, HttpStatus.OK);

        auxRoom = receiverRoom.getIdDormRoom();
        receiverRoom.setIdDormRoom(senderRoom.getIdDormRoom());
        senderRoom.setIdDormRoom(auxRoom);
        studentRoomAffiliationRepository.save(receiverRoom);
        studentRoomAffiliationRepository.save(senderRoom);
        changeRoomRequestRepository.deleteChangeRoomRequestByIdStudentDestination(changeRoomRequest.getIdStudentDestination());

        return new ResponseMessage("You have switched rooms!", 0, HttpServletResponse.SC_OK, HttpStatus.OK);
    }

    @RequestMapping(value = "/decline/{id}", method = RequestMethod.GET)
    public ResponseMessage decline(@PathVariable("id")int id){
        changeRoomRequestRepository.deleteChangeRoomRequestByIdChangeRoomRequest(id);
        return new ResponseMessage("Change Room Request removed!", 0, HttpServletResponse.SC_OK, HttpStatus.OK);
    }

    @RequestMapping(value = "/findbysourcedestination/{idStudentDestination}/{idStudentSource}", method = RequestMethod.GET)
    public List<ChangeRoomRequest> findBySourceDestination(@PathVariable("idStudentDestination") int idStudentDestination, @PathVariable("idStudentSource") int idStudentSource ){
        return Lists.newArrayList(changeRoomRequestRepository.findByIdStudentDestinationAndIdStudentSource(idStudentDestination, idStudentSource));
    }

    @RequestMapping(value = "/findbysource/{idStudentSource}", method = RequestMethod.GET)
    public ChangeRoomRequestCreateDto findBySource(@PathVariable("idStudentSource") int idStudentSource){
        ChangeRoomRequest changeRoomRequest = changeRoomRequestRepository.findByIdStudentSourceAndTimeoutGreaterThan(idStudentSource, new Date());
        if(changeRoomRequest != null)
            return new ChangeRoomRequestCreateDto(changeRoomRequest.getIdChangeRoomRequest(), changeRoomRequest.getIdStudentSource(), studentRepository.findByIdStudent(changeRoomRequest.getIdStudentDestination()).getEmail());
        return new ChangeRoomRequestCreateDto();
    }

    @RequestMapping(value = "/findbydestination/{idStudentDestination}", method = RequestMethod.GET)
    public List<ChangeRoomRequest> findByDestination(@PathVariable("idStudentDestination") int idStudentDestination){
        return Lists.newArrayList(changeRoomRequestRepository.findByIdStudentDestinationAndTimeoutGreaterThan(idStudentDestination, new Date()));
    }

    @RequestMapping(value = "/findbysourcestatus/{idStudentSource}/{status}", method = RequestMethod.GET)
    public List<ChangeRoomRequest> findBySourceStatus(@PathVariable("idStudentSource") int idStudentSource, @PathVariable("status") String status){
        return Lists.newArrayList(changeRoomRequestRepository.findByIdStudentSourceAndStatus(idStudentSource, status));
    }

    @RequestMapping(value = "/findbydestinationstatus/{idStudentDestination}/{status}", method = RequestMethod.GET)
    public List<ChangeRoomRequest> findByDestinationStatus(@PathVariable("idStudentDestination") int idStudentDestination, @PathVariable("status") String status){
        return Lists.newArrayList(changeRoomRequestRepository.findByIdStudentDestinationAndStatus(idStudentDestination, status));
    }

    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/findall", method = RequestMethod.GET)
    public List<ChangeRoomRequest> findAll(){
        return Lists.newArrayList(changeRoomRequestRepository.findAll());
    }
}
