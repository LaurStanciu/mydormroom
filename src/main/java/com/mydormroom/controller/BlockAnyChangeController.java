package com.mydormroom.controller;

import com.google.common.collect.Lists;
import com.mydormroom.model.BlockAnyChange;
import com.mydormroom.repo.BlockAnyChangeRepository;
import com.mydormroom.repo.ChangeRoomRequestRepository;
import com.mydormroom.repo.RoomRepository;
import com.mydormroom.repo.RoomRequestRepository;
import com.mydormroom.utilities.ResponseMessage;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;

/**
 * Created by Laur Stanciu on 2/16/2018.
 */
@RestController
@PreAuthorize("hasAuthority('ROLE_ADMIN')")
@RequestMapping("/api/blockanychanges")
public class BlockAnyChangeController {

    private BlockAnyChangeRepository blockAnyChangeRepository;
    private ChangeRoomRequestRepository changeRoomRequestRepository;
    private RoomRequestRepository roomRequestRepository;
    private RoomRepository roomRepository;

    public BlockAnyChangeController(BlockAnyChangeRepository blockAnyChangeRepository, ChangeRoomRequestRepository changeRoomRequestRepository, RoomRequestRepository roomRequestRepository, RoomRepository roomRepository){
        this.blockAnyChangeRepository = blockAnyChangeRepository;
        this.changeRoomRequestRepository = changeRoomRequestRepository;
        this.roomRequestRepository = roomRequestRepository;
        this.roomRepository = roomRepository;
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json")
    public ResponseMessage save(@RequestBody BlockAnyChange blockAnyChange){
        try {
            blockAnyChangeRepository.deleteAll();
            changeRoomRequestRepository.deleteAll();
            roomRequestRepository.deleteAll();
            roomRepository.updateRoomsRemainingSlots();
            blockAnyChangeRepository.save(blockAnyChange);
            return new ResponseMessage("Registered!", blockAnyChangeRepository.findByIdInitiator(blockAnyChange.getIdInitiator()).getIdBlockAnyChange(), HttpServletResponse.SC_OK, HttpStatus.OK);
        }
        catch(Exception ex){
            return new ResponseMessage(ex.toString(), 0, HttpServletResponse.SC_INTERNAL_SERVER_ERROR, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/findcurrentrestrictions", method = RequestMethod.GET)
    public BlockAnyChange findCurrentRestriction(){
        return blockAnyChangeRepository.findBlockAnyChangeByStartingDateIsLessThanEqualAndFinishingDateGreaterThanEqual(new Date(), new Date());
    }

    @RequestMapping(value = "/deleteall", method = RequestMethod.GET)
    public ResponseMessage deleteAll(){
        blockAnyChangeRepository.deleteAll();
        return new ResponseMessage("Deleted!", 0, HttpServletResponse.SC_OK, HttpStatus.OK);
    }

    @RequestMapping(value = "/findall", method = RequestMethod.GET)
    public List<BlockAnyChange> findAll(){
        return Lists.newArrayList(blockAnyChangeRepository.findAll());
    }
}
