package com.mydormroom.dto;

import java.util.List;

/**
 * Created by Laur Stanciu on 6/18/2018.
 */
public class RoomDto {
    private String dormName;
    private int roomId;
    private int remainingSlots;
    private List<String> colleagues;

    public RoomDto() {}

    public RoomDto(String dormName, int roomId, int remainingSlots, List<String> colleagues){
        this.dormName = dormName;
        this.roomId = roomId;
        this.remainingSlots = remainingSlots;
        this.colleagues = colleagues;
    }

    public String getDormName() {
        return dormName;
    }

    public void setDormName(String dormName) {
        this.dormName = dormName;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public List<String> getColleagues() {
        return colleagues;
    }

    public void setColleagues(List<String> colleagues) {
        this.colleagues = colleagues;
    }

    public int getRemainingSlots() {
        return remainingSlots;
    }

    public void setRemainingSlots(int remainingSlots) {
        this.remainingSlots = remainingSlots;
    }
}
