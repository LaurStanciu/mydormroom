package com.mydormroom.dto;

/**
 * Created by Laur Stanciu on 6/16/2018.
 */
public class RoomRequestAdditionalInformationDto {
    private int idRoomRequestInfo;
    private String colleagueEmail;

    public RoomRequestAdditionalInformationDto () {}

    public RoomRequestAdditionalInformationDto(int idRoomRequestInfo, String colleagueName) {

        this.idRoomRequestInfo = idRoomRequestInfo;
        this.colleagueEmail = colleagueName;
    }

    public int getIdRoomRequestInfo() {
        return idRoomRequestInfo;
    }

    public void setIdRoomRequestInfo(int idRoomRequestInfo) {
        this.idRoomRequestInfo = idRoomRequestInfo;
    }

    public String getColleagueEmail() {
        return colleagueEmail;
    }

    public void setColleagueName(String colleagueName) {
        this.colleagueEmail = colleagueName;
    }
}
