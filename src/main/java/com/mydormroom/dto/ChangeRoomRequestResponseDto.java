package com.mydormroom.dto;

/**
 * Created by Laur Stanciu on 6/16/2018.
 */
public class ChangeRoomRequestResponseDto {
    private int idChangeRoomRequest;
    private String senderEmail;

    public ChangeRoomRequestResponseDto() {}

    public ChangeRoomRequestResponseDto(int idChangeRoomRequest, String senderEmail) {
        this.idChangeRoomRequest = idChangeRoomRequest;
        this.senderEmail = senderEmail;
    }

    public int getIdChangeRoomRequest() {
        return idChangeRoomRequest;
    }

    public void setIdChangeRoomRequest(int idChangeRoomRequest) {
        this.idChangeRoomRequest = idChangeRoomRequest;
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }
}
