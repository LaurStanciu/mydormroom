package com.mydormroom.dto;

import java.util.List;

/**
 * Created by Laur Stanciu on 6/16/2018.
 */
public class NotificationsDto {
    private int updatesNumber;
    private List<RoomRequestAdditionalInformationDto> roomRequestAdditionalInformations;
    private List<ChangeRoomRequestResponseDto> changeRoomRequests;
    private boolean blockChanges;
    private boolean hasRoom;

    public NotificationsDto () {}

    public NotificationsDto(int updatesNumber, List<RoomRequestAdditionalInformationDto> roomRequestAdditionalInformations, List<ChangeRoomRequestResponseDto> changeRoomRequests, boolean blockChanges, boolean hasRoom) {
        this.updatesNumber = updatesNumber;
        this.roomRequestAdditionalInformations = roomRequestAdditionalInformations;
        this.changeRoomRequests = changeRoomRequests;
        this.blockChanges = blockChanges;
        this.hasRoom = hasRoom;
    }

    public List<RoomRequestAdditionalInformationDto> getRoomRequestAdditionalInformations() {
        return roomRequestAdditionalInformations;
    }

    public void setRoomRequestAdditionalInformations(List<RoomRequestAdditionalInformationDto> roomRequestAdditionalInformations) {
        this.roomRequestAdditionalInformations = roomRequestAdditionalInformations;
    }

    public List<ChangeRoomRequestResponseDto> getChangeRoomRequests() {
        return changeRoomRequests;
    }

    public void setChangeRoomRequests(List<ChangeRoomRequestResponseDto> changeRoomRequests) {
        this.changeRoomRequests = changeRoomRequests;
    }

    public boolean isBlockChanges() {
        return blockChanges;
    }

    public void setBlockChanges(boolean blockChanges) {
        this.blockChanges = blockChanges;
    }

    public int getUpdatesNumber() {
        return updatesNumber;
    }

    public void setUpdatesNumber(int updatesNumber) {
        this.updatesNumber = updatesNumber;
    }

    public boolean isHasRoom() {
        return hasRoom;
    }

    public void setHasRoom(boolean hasRoom) {
        this.hasRoom = hasRoom;
    }
}
