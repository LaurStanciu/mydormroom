package com.mydormroom.dto;

/**
 * Created by Laur Stanciu on 6/17/2018.
 */
public class ChangeRoomRequestCreateDto {
    private int idChangeRoomRequest;
    private int senderIdStudent;
    private String receiverEmail;

    public ChangeRoomRequestCreateDto() {}

    public ChangeRoomRequestCreateDto(int idChangeRoomRequest, int senderIdStudent, String receiverEmail) {
        this.idChangeRoomRequest = idChangeRoomRequest;
        this.senderIdStudent = senderIdStudent;
        this.receiverEmail = receiverEmail;
    }

    public int getIdChangeRoomRequest() {
        return idChangeRoomRequest;
    }

    public void setIdChangeRoomRequest(int idChangeRoomRequest) {
        this.idChangeRoomRequest = idChangeRoomRequest;
    }

    public int getSenderIdStudent() {
        return senderIdStudent;
    }

    public void setSenderIdStudent(int senderIdStudent) {
        this.senderIdStudent = senderIdStudent;
    }

    public String getReceiverEmail() {
        return receiverEmail;
    }

    public void setReceiverEmail(String receiverEmail) {
        this.receiverEmail = receiverEmail;
    }
}
