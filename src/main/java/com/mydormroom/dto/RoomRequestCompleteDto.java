package com.mydormroom.dto;

import com.mydormroom.model.Room;
import com.mydormroom.model.RoomRequestAdditionalInformation;

import java.util.List;

/**
 * Created by Laur on 5/28/2018.
 */
public class RoomRequestCompleteDto {
    private int idRoomRequest;
    private int idStudent;
    private Room idDormRoom;
    private List<String> colleagues;

    public RoomRequestCompleteDto(int idRoomRequest, int idStudent,
                                  Room idDormRoom, List<String> colleagues) {
        this.idRoomRequest = idRoomRequest;
        this.idStudent = idStudent;
        this.idDormRoom = idDormRoom;
        this.colleagues = colleagues;
    }

    public RoomRequestCompleteDto () {}

    public RoomRequestCompleteDto(int idStudent, Room idDormRoom, List<String> colleagues) {
        this.idStudent = idStudent;
        this.idDormRoom = idDormRoom;
        this.colleagues = colleagues;
    }



    public int getIdRoomRequest() {
        return idRoomRequest;
    }

    public void setIdRoomRequest(int idRoomRequest) {
        this.idRoomRequest = idRoomRequest;
    }

    public int getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    public Room getIdDormRoom() {
        return idDormRoom;
    }

    public void setIdDormRoom(Room idDormRoom) {
        this.idDormRoom = idDormRoom;
    }

    public List<String> getColleagues() {
        return colleagues;
    }

    public void setColleagues(List<String> colleagues) {
        this.colleagues = colleagues;
    }
}
