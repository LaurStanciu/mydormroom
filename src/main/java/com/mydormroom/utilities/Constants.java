package com.mydormroom.utilities;

/**
 * Created by Laur on 2/10/2018.
 */
public class Constants {
    public static int HOUR = 3600*1000;
    public static String ROLE_ADMIN = "ROLE_ADMIN";
    public static boolean USE_BCRYPT = false;
}
