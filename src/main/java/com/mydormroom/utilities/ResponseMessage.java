package com.mydormroom.utilities;

import org.springframework.http.HttpStatus;

import java.util.Date;

/**
 * Created by Laur Stanciu on 4/2/2018.
 */
public class ResponseMessage {
    private int extraData;
    private HttpStatus status;
    private String message;
    private int code;
    private Date timestamp;

    public ResponseMessage(String message,int extraData, int code, HttpStatus status) {
        this.extraData = extraData;
        this.message = message;
        this.code = code;
        this.status = status;
        this.timestamp = new java.util.Date();
    }

    public Integer getStatus() {
        return status.value();
    }

    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public int getExtraData() {
        return extraData;
    }

    public void setExtraData(int extraData) {
        this.extraData = extraData;
    }
}
