package com.mydormroom.utilities;

/**
 * Created by Laur on 2/26/2018.
 */
public class SecurityConstants {
    public static final String SECRET = "MyDormRoom";
    public static final long EXPIRATION_TIME = 30*60*1000;
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final int MAX_NUMBER_OF_ATTEMPTS = 3;
    public static final int RESET_NUMBER_OF_ATTEMPTS = 0;
    public static int BLOCK_TIME = 10*60*1000;
}
