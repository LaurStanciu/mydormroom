package com.mydormroom.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Laur on 2/11/2018.
 */
@Entity
@Table(name = "Studentsroomsaffiliation")
public class StudentRoomAffiliation implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idstudentroomaffi")
    private int idStudentRoomAffi;

    @Column(name = "iddormroom")
    private int idDormRoom;

    @Column(name = "idstudent", unique = true)
    private int idStudent;

    @Column(name = "setindate")
    private Date setInDate = new Date();

    protected StudentRoomAffiliation(){}

    public StudentRoomAffiliation(int idDormRoom, int idStudent){
        this.idDormRoom = idDormRoom;
        this.idStudent = idStudent;
    }

    public int getIdStudentRoomAffi() {
        return idStudentRoomAffi;
    }

    public int getIdDormRoom() {
        return idDormRoom;
    }

    public void setIdDormRoom(int idDormRoom) {
        this.idDormRoom = idDormRoom;
    }

    public int getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    public Date getSetInDate() {
        return setInDate;
    }
}
