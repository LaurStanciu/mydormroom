package com.mydormroom.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Laur on 2/11/2018.
 */
@Entity
@Table(name = "Roomrequestsadditionalinformation", uniqueConstraints = {@UniqueConstraint(columnNames = {"idstudent" , "idcolleague"})})
public class RoomRequestAdditionalInformation implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idroomrequestinfo")
    private int idRoomRequestInfo;

    @Column(name = "idstudent")
    private int idStudent;

    @Column(name = "idcolleague")
    private int idColleague;

    @Column(name = "status")
    private boolean status;

    @Column(name = "timestamp")
    private  Date timestamp = new Date();

    protected RoomRequestAdditionalInformation(){}

    public RoomRequestAdditionalInformation(int idStudent, int idColleague){
        this.idStudent = idStudent;
        this.idColleague = idColleague;
    }

    public int getIdRoomRequestInfo() {
        return idRoomRequestInfo;
    }

    public int getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    public int getIdColleague() {
        return idColleague;
    }

    public void setIdColleague(int idColleague) {
        this.idColleague = idColleague;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
