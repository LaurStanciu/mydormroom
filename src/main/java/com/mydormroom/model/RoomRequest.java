package com.mydormroom.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Laur on 2/10/2018.
 */

@Entity
@Table(name = "Roomrequests", uniqueConstraints = {@UniqueConstraint(columnNames = {"idstudent"})})
public class RoomRequest implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idroomrequest")
    private int idRoomRequest;

    @Column(name = "idstudent")
    private int idStudent;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "iddormroom")
    private Room idDormRoom;

    @Column(name = "timestamp")
    private Date timestamp = new Date();

    protected RoomRequest(){}

    public RoomRequest(int idRoomRequest, int idStudent, Room idDormRoom){
        this.idRoomRequest = idRoomRequest;
        this.idStudent = idStudent;
        this.idDormRoom = idDormRoom;
    }

    public RoomRequest(int idStudent, Room idDormRoom){
        this.idStudent = idStudent;
        this.idDormRoom = idDormRoom;
    }

    public int getIdRoomRequest() {
        return idRoomRequest;
    }

    public int getIdStudent() {
        return idStudent;
    }

    public void setIdStudent(int idStudent) {
        this.idStudent = idStudent;
    }

    public Room getIdDormRoom() {
        return idDormRoom;
    }

    public void setIdDormRoom(Room idDormRoom) {
        this.idDormRoom = idDormRoom;
    }

    public Date getTimestamp() {
        return timestamp;
    }
}
