package com.mydormroom.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Laur on 2/3/2018.
 */
@Entity
@Table(name = "Dorms")
public class Dorm implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="iddorm")
    private int idDorm;

    @Column(name = "name", unique = true)
    private String name;

    @Column(name = "numberofrooms")
    private int numberOfRooms;

    @Column(name = "timestamp")
    private Date timestamp = new Date();

    protected Dorm(){}

    public Dorm(String name, int numberOfRooms){
        this.name = name;
        this.numberOfRooms = numberOfRooms;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberOfRooms() {
        return numberOfRooms;
    }

    public void setNumberOfRooms(int numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }

    public int getIdDorm() {
        return idDorm;
    }

    public Date getTimestamp() {
        return timestamp;
    }
}
