package com.mydormroom.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Laur on 2/10/2018.
 */
@Entity
@Table(name = "Blockanychanges")
public class BlockAnyChange implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idblockanychange")
    private int idBlockAnyChange;

    @Column(name = "idinitiator")
    private int idInitiator;

    @Column(name = "startingdate")
    private Date startingDate;

    @Column(name = "finishingdate")
    private Date finishingDate;

    @Column(name = "active")
    private boolean active;

    @Column(name = "timestamp")
    private Date timestamp = new Date();

    protected BlockAnyChange(){}

    public BlockAnyChange(int idInitiator, Date startingDate, Date finishingDate, boolean active){
        this.idInitiator = idInitiator;
        this.startingDate = startingDate;
        this.finishingDate = finishingDate;
        this.active = active;
    }

    public int getIdBlockAnyChange() {
        return idBlockAnyChange;
    }

    public int getIdInitiator() {
        return idInitiator;
    }

    public void setIdInitiator(int idInitiator) {
        this.idInitiator = idInitiator;
    }

    public Date getStartingDate() {
        return startingDate;
    }

    public void setStartingDate(Date startingDate) {
        this.startingDate = startingDate;
    }

    public Date getFinishingDate() {
        return finishingDate;
    }

    public void setFinishingDate(Date finishingDate) {
        this.finishingDate = finishingDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Date getTimestamp() {
        return timestamp;
    }
}
