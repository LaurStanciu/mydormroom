package com.mydormroom.model;

import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Laur on 2/10/2018.
 */
@Entity
@Table(name = "Students", uniqueConstraints = {@UniqueConstraint(columnNames = {"cnp"})})
public class Student implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idstudent")
    private int idStudent;

    @Column(name = "idaccount") // fk cu account
    private int idAccount;

    @Column(name = "lastname")
    private String lastName;

    @Column(name = "firstname")
    private String firstName;

    @Column(name = "cnp")
    private String cnp;

    @Email
    @Column(name = "email", unique = true)
    private String email;

    @Column(name = "dateofbirth")
    private Date dateOfBirth;

    @Column(name = "idyearofstudy")
    private int idYearOfStudy;

    @Column(name = "grade")
    private double grade;

    @Column(name = "failures")
    private int failures;

    @Column(name = "roomrequest")
    private boolean roomRequest;

    @Column(name = "timestamp")
    private Date timestamp = new Date();

    protected Student(){}

    public Student(int idAccount, String lastName, String firstName, String cnp, String email, Date dateOfBirth, int idYearOfStudy,
                   float grade, int failures, boolean roomRequest){
        this.idAccount = idAccount;
        this.lastName = lastName;
        this.firstName = firstName;
        this.cnp = cnp;
        this.email = email;
        this.dateOfBirth = dateOfBirth;
        this.idYearOfStudy = idYearOfStudy;
        this.grade = grade;
        this.failures = failures;
        this.roomRequest = roomRequest;
    }


    public int getIdStudent() {
        return idStudent;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getCnp() {
        return cnp;
    }

    public void setCnp(String cnp) {
        this.cnp = cnp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getIdYearOfStudy() {
        return idYearOfStudy;
    }

    public void setIdYearOfStudy(int idYearOfStudy) {
        this.idYearOfStudy = idYearOfStudy;
    }

    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }

    public int getFailures() {
        return failures;
    }

    public void setFailures(int failures) {
        this.failures = failures;
    }

    public boolean isRoomRequest() {
        return roomRequest;
    }

    public void setRoomRequest(boolean roomRequest) {
        this.roomRequest = roomRequest;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public int getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(int idAccount) {
        this.idAccount = idAccount;
    }
}
