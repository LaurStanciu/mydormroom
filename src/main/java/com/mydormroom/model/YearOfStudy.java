package com.mydormroom.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Laur on 2/10/2018.
 */

@Entity
@Table(name = "Yearsofstudy")
public class YearOfStudy implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idyearofstudy")
    private int idYearOfStudy;

    @Column(name = "name", unique = true)
    private String name;

    @Column(name = "priority", unique = true)
    private int priority;

    @Column(name = "timestamp")
    private Date timestamp = new Date();

    protected YearOfStudy(){}

    public YearOfStudy(String name, int priority) {
        this.name = name;
        this.priority = priority;
    }

    public int getIdYearOfStudy() {
        return idYearOfStudy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public Date getTimestamp() {
        return timestamp;
    }
}
