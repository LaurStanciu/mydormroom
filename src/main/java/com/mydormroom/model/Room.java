package com.mydormroom.model;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Laur on 2/10/2018.
 */

@Entity
@Table(name = "Rooms", uniqueConstraints={@UniqueConstraint(columnNames = {"iddorm" , "idroom"})})
public class Room implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "iddormroom")
    private int idDormRoom;
    
    @Column(name = "iddorm")
    private int idDorm;

    @Column(name = "idroom")
    private int idRoom;

    @Column(name = "capacity")
    private int capacity;

    @Column(name = "remainingslots")
    private int remainingSlots;

    @Column(name = "nottaken")
    private int notTaken;

    @Column(name = "status")
    private boolean status;

    @Column(name = "timestamp")
    private Date timestamp = new Date();

    protected Room(){}

    public Room(int idDorm, int idRoom, int capacity, int remainingSlots, int notTaken, boolean status){
        this.idDorm = idDorm;
        this.idRoom = idRoom;
        this.capacity = capacity;
        this.remainingSlots = remainingSlots;
        this.notTaken = notTaken;
        this.status = status;
    }

    public int getIdDormRoom() {
        return idDormRoom;
    }

    public int getIdDorm() {
        return idDorm;
    }

    public void setIdDorm(int idDorm) {
        this.idDorm = idDorm;
    }

    public int getIdRoom() {
        return idRoom;
    }

    public void setIdRoom(int idRoom) {
        this.idRoom = idRoom;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public int getRemainingSlots() {
        return remainingSlots;
    }

    public void setRemainingSlots(int remainingSlots) {
        this.remainingSlots = remainingSlots;
    }

    public int getNotTaken() {
        return notTaken;
    }

    public void setNotTaken(int notTaken) {
        this.notTaken = notTaken;
    }
}
