package com.mydormroom.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Laur on 2/10/2018.
 */

@Entity
@Table(name = "Accounts")
public class Account implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idaccount")
    private int idAccount;

    @Column(name = "username", unique = true)
    private String username;

    @Column(name = "password")
    private String password;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "idrole")
    private Role idRole;

    @Column(name = "attempts")
    private int attempts;

    @Column(name = "active")
    private boolean active = false;

    @Column(name = "resetattemptsdate")
    private Date resetAttemptsDate = new Date();

    @Column(name = "unlockaccountdate")
    private Date unlockAccountDate = new Date();

    @Column(name = "timestamp")
    private Date timestamp = new Date();

    protected Account(){}

    public Account(String username, String password, Role idRole, int attempts, boolean active, Date resetAttemptsDate, Date unlockAccountDate){
        this.username = username;
        this.password = password;
        this.idRole = idRole;
        this.attempts = attempts;
        this.active = active;
        this.resetAttemptsDate = resetAttemptsDate;
        this.unlockAccountDate = unlockAccountDate;
    }

    public int getIdAccount() {
        return idAccount;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getIdRole() {
        return idRole;
    }

    public void setIdRole(Role idRole) {
        this.idRole = idRole;
    }

    public int getAttempts() {
        return attempts;
    }

    public void setAttempts(int attempts) {
        this.attempts = attempts;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public Date getResetAttemptsDate() {
        return resetAttemptsDate;
    }

    public void setResetAttemptsDate(Date resetAttemptsDate) {
        this.resetAttemptsDate = resetAttemptsDate;
    }

    public Date getUnlockAccountDate() {
        return unlockAccountDate;
    }

    public void setUnlockAccountDate(Date unlockAccountDate) {
        this.unlockAccountDate = unlockAccountDate;
    }

    public Date getTimestamp() {
        return timestamp;
    }
}
