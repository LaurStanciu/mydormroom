package com.mydormroom.model;

import com.mydormroom.utilities.Constants;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import static com.mydormroom.utilities.Constants.HOUR;

/**
 * Created by Laur on 2/10/2018.
 */
@Entity
@Table(name = "Changeroomrequests")
public class ChangeRoomRequest implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idchangeroomrequest")
    private int idChangeRoomRequest;

    @Column(name = "idstudentsource")
    private int idStudentSource;

    @Column(name = "idstudentdestination")
    private int idStudentDestination;

    @Column(name = "status")
    private boolean status;

    @Column(name = "timeout")
    private Date timeout = new Date(new Date().getTime() + 12*HOUR);

    @Column(name = "timestamp")
    private Date timestamp = new Date();

    protected ChangeRoomRequest(){}

    public ChangeRoomRequest(int idStudentSource, int idStudentDestination, boolean status){
        this.idStudentSource = idStudentSource;
        this.idStudentDestination = idStudentDestination;
        this.status = status;
    }

    public int getIdChangeRoomRequest() {
        return idChangeRoomRequest;
    }

    public int getIdStudentSource() {
        return idStudentSource;
    }

    public void setIdStudentSource(int idStudentSource) {
        this.idStudentSource = idStudentSource;
    }

    public int getIdStudentDestination() {
        return idStudentDestination;
    }

    public void setIdStudentDestination(int idStudentDestination) {
        this.idStudentDestination = idStudentDestination;
    }

    public Date getTimeout() {
        return timeout;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
