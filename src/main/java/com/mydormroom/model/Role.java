package com.mydormroom.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Laur Stanciu on 3/5/2018.
 */
@Entity
@Table(name = "Roles")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idrole")
    private int idRole;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "timestamp")
    private Date timestamp = new Date();

    protected Role(){}

    public Role(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public int getIdRole() {
        return idRole;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getTimestamp() {
        return timestamp;
    }
}
