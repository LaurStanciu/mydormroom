package com.mydormroom.security;

/**
 * Created by Laur on 2/26/2018.
 */

import com.mydormroom.repo.AccountRepository;
import com.mydormroom.repo.StudentRepository;
import com.mydormroom.services.AccountService;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import static com.mydormroom.utilities.Constants.USE_BCRYPT;
import static com.mydormroom.utilities.SecurityConstants.*;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled=true)
public class WebSecurity extends WebSecurityConfigurerAdapter {

    private AccountService accountService;
    private AccountRepository accountRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private ITokenHelper tokenHelper;
    private StudentRepository studentRepository;

    public WebSecurity(AccountService accountService, AccountRepository accountRepository, BCryptPasswordEncoder bCryptPasswordEncoder, ITokenHelper tokenHelper, StudentRepository studentRepository) {
        this.accountService = accountService;
        this.accountRepository = accountRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.tokenHelper = tokenHelper;
        this.studentRepository = studentRepository;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        JWTAuthenticationFilter customFilter = new JWTAuthenticationFilter(authenticationManager(),
                accountRepository, studentRepository);
        customFilter.setFilterProcessesUrl("/api/login");
        http.addFilterBefore(customFilter, UsernamePasswordAuthenticationFilter.class);

        http.csrf().disable().authorizeRequests()
                .antMatchers("/*", "/static/**").permitAll()
                .anyRequest().authenticated()
                .and()
                .addFilter(new JWTAuthorizationFilter(authenticationManager(), accountRepository, tokenHelper))
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        if(USE_BCRYPT)
            auth.userDetailsService(accountService).passwordEncoder(bCryptPasswordEncoder);
        else auth.userDetailsService(accountService);
    }
}
