package com.mydormroom.security;

import io.jsonwebtoken.Claims;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Laur Stanciu on 4/6/2018.
 */
public interface ITokenHelper {
    String refreshToken(HttpServletRequest req, HttpServletResponse res);
    Claims parseToken(HttpServletRequest request);
}
