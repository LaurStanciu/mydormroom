package com.mydormroom.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mydormroom.model.Account;
import com.mydormroom.model.Student;
import com.mydormroom.repo.AccountRepository;
import com.mydormroom.repo.StudentRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import static com.mydormroom.utilities.SecurityConstants.*;

/**
 * Created by Laur on 2/26/2018.
 */
public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private Account creds;
    private AccountRepository accountRepository;
    private AuthenticationManager authenticationManager;
    private StudentRepository studentRepository;

    public JWTAuthenticationFilter (AuthenticationManager authenticationManager, AccountRepository accountRepository, StudentRepository studentRepository){
        this.authenticationManager = authenticationManager;
        this.accountRepository = accountRepository;
        this.studentRepository = studentRepository;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req,
                                                HttpServletResponse res) throws AuthenticationException {
        try {
            creds = new ObjectMapper()
                    .readValue(req.getInputStream(), Account.class);
            Account repoAccount = accountRepository.findByUsername(creds.getUsername());
            if (repoAccount != null) {
                if (repoAccount.getResetAttemptsDate().before(new Date())) {
                    repoAccount.setAttempts(RESET_NUMBER_OF_ATTEMPTS);
                    accountRepository.save(repoAccount);
                }
            }
            Authentication auth = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            creds.getUsername(),
                            creds.getPassword(),
                            new ArrayList<>()));
            return auth;

        }catch (BadCredentialsException e) {
            Account repoAccount = accountRepository.findByUsername(creds.getUsername());
            if(repoAccount != null)
                if(repoAccount.getAttempts() == MAX_NUMBER_OF_ATTEMPTS - 1) {
                    repoAccount.setUnlockAccountDate(new Date(new Date().getTime() + BLOCK_TIME));
                    repoAccount.setAttempts(RESET_NUMBER_OF_ATTEMPTS);
                    accountRepository.save(repoAccount);
                }
                else {
                    repoAccount.setAttempts(repoAccount.getAttempts() + 1);
                    repoAccount.setResetAttemptsDate(new Date(new Date().getTime() + BLOCK_TIME));
                    accountRepository.save(repoAccount);
                }
            throw e;
        }catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req,
                                            HttpServletResponse res,
                                            FilterChain chain,
                                            Authentication auth) throws IOException, ServletException {

        Account repoAccount = accountRepository.findByUsername(creds.getUsername());
        repoAccount.setAttempts(RESET_NUMBER_OF_ATTEMPTS);
        accountRepository.save(repoAccount);
        Student student = studentRepository.findByIdAccount(repoAccount.getIdAccount());

        Claims claims = Jwts.claims().setSubject(((User) auth.getPrincipal()).getUsername());
        claims.put("scopes", Arrays.asList(new SimpleGrantedAuthority(repoAccount.getIdRole().getName())));
        claims.put("identification", repoAccount.getIdAccount());
        if(student != null) {
            claims.put("lastName", student.getLastName());
            claims.put("firstName", student.getFirstName());
            claims.put("studentIdentification", student.getIdStudent());
        }
        String token = Jwts.builder()
                .setClaims(claims)
                .setExpiration(new Date(new Date().getTime() + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SECRET.getBytes())
                .compact();
        res.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
    }
}

