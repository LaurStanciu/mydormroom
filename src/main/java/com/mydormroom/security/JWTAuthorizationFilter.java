package com.mydormroom.security;

/**
 * Created by Laur on 2/26/2018.
 */

import com.mydormroom.repo.AccountRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.mydormroom.utilities.SecurityConstants.*;


public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    private AccountRepository accountRepository;
    private ITokenHelper tokenHelper;

    public JWTAuthorizationFilter(AuthenticationManager authManager, AccountRepository accountRepository, ITokenHelper tokenHelper) {
        super(authManager);
        this.accountRepository = accountRepository;
        this.tokenHelper = tokenHelper;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain chain) throws IOException, ServletException {
        String header = request.getHeader(HEADER_STRING);

        if (header == null || !header.startsWith(TOKEN_PREFIX)) {
            chain.doFilter(request, response);
            return;
        }

        UsernamePasswordAuthenticationToken authentication = getAuthentication(request);

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String token = tokenHelper.refreshToken(request, response);
        response.addHeader(HEADER_STRING, TOKEN_PREFIX + token);

        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
        String token = request.getHeader(HEADER_STRING);
        if (token != null) {
            // parse the token.
            Claims claims = tokenHelper.parseToken(request);

            if (claims != null) {

                List<GrantedAuthority> authorities = new ArrayList<>();

                authorities.add(new SimpleGrantedAuthority(accountRepository.findByUsername(claims.getSubject()).getIdRole().getName()));

                return new UsernamePasswordAuthenticationToken(claims.getSubject(), null, authorities);
            }
            return null;
        }
        return null;
    }
}
