package com.mydormroom.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.Date;

import static com.mydormroom.utilities.SecurityConstants.*;

/**
 * Created by Laur Stanciu on 4/6/2018.
 */
@Service
public class TokenHelper implements ITokenHelper{

    @Override
    public String refreshToken(HttpServletRequest request,
                                HttpServletResponse response) {
        String token = Jwts.builder()
                .setClaims(parseToken(request))
                .setExpiration(new Date(new Date().getTime() + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SECRET.getBytes())
                .compact();
        return token;
    }

    @Override
    public Claims parseToken(HttpServletRequest request) {
        String token = request.getHeader(HEADER_STRING);

        Claims claims = Jwts.parser()
                .setSigningKey(SECRET.getBytes())
                .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                .getBody();
        return claims;
    }
}
