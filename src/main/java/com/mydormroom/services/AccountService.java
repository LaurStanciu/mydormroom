package com.mydormroom.services;

import com.mydormroom.model.Account;
import com.mydormroom.repo.AccountRepository;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Date;

import static java.util.Collections.emptyList;

/**
 * Created by Laur Stanciu on 2/28/2018.
 */
@Service
public class AccountService implements UserDetailsService {

    private AccountRepository accountRepository;

    public AccountService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Account account = accountRepository.findByUsername(username);
        if (account == null) {
            throw new UsernameNotFoundException(username);
        }
        return new User(account.getUsername(), account.getPassword(),account.isActive(),true,
                true, account.getUnlockAccountDate().before(new Date()), emptyList());
    }
}
