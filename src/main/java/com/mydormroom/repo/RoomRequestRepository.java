package com.mydormroom.repo;

import com.mydormroom.model.Room;
import com.mydormroom.model.RoomRequest;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Laur Stanciu on 2/17/2018.
 */
public interface RoomRequestRepository extends JpaRepository<RoomRequest, Integer> {
    List<RoomRequest> findByIdDormRoom(Room idDormRoom);
    RoomRequest findByIdStudent(int idStudent);
}
