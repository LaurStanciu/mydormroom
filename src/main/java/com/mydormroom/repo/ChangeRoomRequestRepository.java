package com.mydormroom.repo;

import com.mydormroom.model.ChangeRoomRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by Laur Stanciu on 2/17/2018.
 */
public interface ChangeRoomRequestRepository extends JpaRepository<ChangeRoomRequest, Integer> {
    List<ChangeRoomRequest> findByIdStudentDestinationAndStatus(int idStudentDestination, boolean status);
    List<ChangeRoomRequest> findByIdStudentDestinationAndStatus(int idStudentDestination, String status);
    List<ChangeRoomRequest> findByIdStudentDestinationAndTimeoutGreaterThan(int idStudentDestination, Date currentDate);
    List<ChangeRoomRequest> findByIdStudentSourceAndStatus(int idStudentSource, boolean status);
    List<ChangeRoomRequest> findByIdStudentSourceAndStatus(int idStudentSource, String status);
    List<ChangeRoomRequest> findByIdStudentSource(int idStudentSource);
    ChangeRoomRequest findByIdStudentSourceAndTimeoutGreaterThan(int idStudentSource, Date date);
    List<ChangeRoomRequest> findByIdStudentDestinationAndIdStudentSource(int idStudentDestination, int idStudentSource);
    @Transactional
    void deleteChangeRoomRequestByIdStudentDestination(int idStudentDestination);
    @Transactional
    void deleteChangeRoomRequestByIdChangeRoomRequest(int idChangeRoomRequest);
    @Transactional
    void deleteChangeRoomRequestByIdStudentSource(int idStudentSource);
}
