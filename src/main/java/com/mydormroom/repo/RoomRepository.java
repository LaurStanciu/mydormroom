package com.mydormroom.repo;

import com.mydormroom.model.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Laur on 2/10/2018.
 */

public interface RoomRepository extends JpaRepository<Room, Integer> {
    Room findByIdDormAndIdRoom(int IdDorm, int IdRoom);
    List<Room> findByIdDormOrderByIdRoom(int IdDorm);
    Room findByIdDormRoom(int IdDormRoom);
    List<Room> findByRemainingSlotsGreaterThan(int limit);
    List<Room> findByIdDormAndRemainingSlotsGreaterThan(int IdDorm, int remainingSlots);

    @Modifying
    @Transactional
    @Query("UPDATE Room SET remainingSlots = notTaken")
    void updateRoomsRemainingSlots();
}
