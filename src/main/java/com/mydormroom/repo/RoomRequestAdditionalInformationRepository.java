package com.mydormroom.repo;

import com.mydormroom.model.Room;
import com.mydormroom.model.RoomRequestAdditionalInformation;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Laur Stanciu on 2/17/2018.
 */
public interface RoomRequestAdditionalInformationRepository
        extends JpaRepository<RoomRequestAdditionalInformation, Integer> {
    List<RoomRequestAdditionalInformation> findByIdStudent(int idStudent);
    RoomRequestAdditionalInformation findByIdStudentAndIdColleague(int idStudent,
                                                                   int idColleague);
    List<RoomRequestAdditionalInformation> findByIdColleagueAndStatus(int idColleague,
                                                                    boolean status);
    RoomRequestAdditionalInformation findByIdRoomRequestInfo(int idRoomRequestInfo);
    @Transactional
    void deleteRoomRequestAdditionalInformationByIdRoomRequestInfo(int idRoomRequestInfo);
    @Transactional
    void deleteRoomRequestAdditionalInformationByIdStudent(int idStudent);
    @Transactional
    void deleteRoomRequestAdditionalInformationByIdColleague(int idColleague);
}
