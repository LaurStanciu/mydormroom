package com.mydormroom.repo;

import com.mydormroom.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Laur Stanciu on 2/17/2018.
 */
public interface StudentRepository extends JpaRepository<Student, Integer> {
    List<Student> findByLastNameContainsAndFirstNameContains(String lastName, String firstName);
    Student findByIdAccount(int idAccount);
    Student findByEmail(String email);
    Student findByIdStudent(int idStudent);
    List<Student> findByIdYearOfStudy(int idYearOfStudy);
}
