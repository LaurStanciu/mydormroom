package com.mydormroom.repo;

import com.mydormroom.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Laur Stanciu on 3/6/2018.
 */
public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findByName(String nume);
}
