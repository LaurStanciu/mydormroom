package com.mydormroom.repo;

import com.mydormroom.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Laur on 2/14/2018.
 */
public interface AccountRepository extends JpaRepository<Account, Integer> {
    Account findByUsername(String username);
}
