package com.mydormroom.repo;

import com.mydormroom.model.StudentRoomAffiliation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Laur Stanciu on 2/17/2018.
 */
public interface StudentRoomAffiliationRepository extends JpaRepository<StudentRoomAffiliation, Integer> {
    List<StudentRoomAffiliation> findByIdDormRoom(int idDormRoom);
    StudentRoomAffiliation findByIdStudent(int idStudent);
}
