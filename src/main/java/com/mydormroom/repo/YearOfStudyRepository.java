package com.mydormroom.repo;

import com.mydormroom.model.YearOfStudy;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Laur Stanciu on 2/16/2018.
 */
public interface YearOfStudyRepository extends JpaRepository<YearOfStudy, Integer> {
    List<YearOfStudy> findAllByOrderByPriorityDesc();
}
