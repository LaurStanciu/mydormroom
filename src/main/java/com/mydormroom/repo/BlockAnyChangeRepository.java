package com.mydormroom.repo;

import com.mydormroom.model.BlockAnyChange;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;
import java.util.List;

/**
 * Created by Laur Stanciu on 2/16/2018.
 */
public interface BlockAnyChangeRepository extends JpaRepository<BlockAnyChange, Integer> {
    BlockAnyChange findBlockAnyChangeByStartingDateIsLessThanEqualAndFinishingDateGreaterThanEqual(Date date1, Date date2);
    BlockAnyChange findByIdInitiator(int idInitiator);
    int countBlockAnyChangeByStartingDateIsLessThanEqualAndFinishingDateGreaterThanEqual(Date date1, Date date2);
}
