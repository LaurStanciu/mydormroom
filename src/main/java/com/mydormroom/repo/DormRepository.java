package com.mydormroom.repo;

import com.mydormroom.model.Dorm;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Laur on 2/3/2018.
 */

public interface DormRepository extends JpaRepository<Dorm, Integer> {
    List<Dorm> findByNameContains(String name);
}
