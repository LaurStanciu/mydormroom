import * as types from './mutation-types'
import axios from 'axios'

const mutations = {
  [types.LOGIN] (state) {
    state.auth.pending = true
  },
  [types.LOGIN_SUCCESS] (state, data) {
    const token = data.token
    state.auth.isLoggedIn = true
    state.auth.pending = false
    state.auth.token = token
    localStorage.setItem('JWT', token)
    axios.defaults.headers.common['Authorization'] = token
  },
  [types.LOGOUT] (state) {
    localStorage.removeItem('JWT')
    delete axios.defaults.headers.common['Authorization']
    state.auth.isLoggedIn = false
  },
  [types.REFRESH_TOKEN] (state, data) {
    const token = data.token
    state.auth.isLoggedIn = true
    state.auth.pending = false
    state.auth.token = token
    localStorage.setItem('JWT', token)
    axios.defaults.headers.common['Authorization'] = token
  },
  [types.UPDATES] (state, data) {
    console.log('data')
    console.log(data)
    const updates = data
    state.updates = updates
  }
}

export default mutations
