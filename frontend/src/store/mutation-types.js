export const LOGIN = 'LOGIN'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGOUT = 'LOGOUT'

export const UPDATES = 'UPDATES'

export const REFRESH_TOKEN = 'REFRESH_TOKEN'
