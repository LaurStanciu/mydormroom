import * as types from './mutation-types'
import router from '../router'

import LoginService from '../services/LoginService'

let loginService = new LoginService()

const login = ({ commit }, creds) => {
  commit(types.LOGIN) // show spinner
  return loginService.logInUser(creds.username, creds.password)
}

const refreshToken = ({ commit }, token) => {
  commit(types.REFRESH_TOKEN)
}

const logout = ({ commit }) => {
  commit(types.LOGOUT)
  router.push('/login')
}

const updates = ({ commit }, data) => {
  commit(types.UPDATES)
}

export default {
  [types.LOGIN]: login,
  [types.LOGOUT]: logout,
  [types.REFRESH_TOKEN]: refreshToken,
  [types.UPDATES]: updates
}
