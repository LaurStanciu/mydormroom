import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'
import * as types from '../store/mutation-types'
import CheckUpdatesService from '../services/CheckUpdatesService'
import JwtDecodeService from '../services/JwtDecodeService'

let checkUpdatesService = new CheckUpdatesService()
let jwtDecodeService = new JwtDecodeService()

const hasToken = (to, from, next) => {
  const token = localStorage.getItem('JWT')
  if (token) {
    store.commit(types.LOGIN_SUCCESS, { token })
    router.push('/home')
  } else {
    next()
  }
}

const isAdmin = (to, from, next) => {
  let obj = jwtDecodeService.decode(localStorage.getItem('JWT').substr(7))
  if (obj.scopes[0].authority === 'ROLE_ADMIN') {
    next()
  } else {
    router.push(from)
  }
}

const isBlockChanges = (to, from, next) => {
  let updates = store.getters.getUpdates
  if (!updates.blockChanges) {
    next()
  } else {
    router.push('/home')
  }
}

const requireAuth = (to, from, next) => {
  checkUpdatesService.checkUpdates()
  .then(response => {
    if (store.getters.isLoggedIn) {
      next()
    } else {
      router.push('/login')
    }
  })
  .catch(error => {
    console.log(error)
    router.push('/login')
  })
}

const checkIfPageExists = (to, from, next) => {
  router.push(from)
}

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: () => import('@/components/Login'),
      beforeEnter: hasToken
    },
    {
      path: '/',
      name: 'Root',
      component: () => import('@/components/Root'),
      beforeEnter: requireAuth,
      redirect: '/home',
      children: [
        {
          path: '/home',
          name: 'Home',
          component: () => import('@/components/Home'),
          beforeEnter: requireAuth
        },
        {
          path: '/inbox',
          name: 'Inbox',
          component: () => import('@/components/Inbox'),
          beforeEnter: requireAuth
        },
        {
          path: '/roomrequest',
          name: 'RoomRequest',
          component: () => import('@/components/RoomRequest'),
          beforeEnter: requireAuth && isBlockChanges
        },
        {
          path: '/changeroomrequest',
          name: 'ChangeRoomRequest',
          component: () => import('@/components/ChangeRoomRequest'),
          beforeEnter: requireAuth && isBlockChanges
        },
        {
          path: '/roominfo',
          name: 'RoomInfo',
          component: () => import('@/components/RoomInfo'),
          beforeEnter: requireAuth
        },
        {
          path: '/blockanychanges',
          name: 'BlockAnyChanges',
          component: () => import('@/components/BlockAnyChanges'),
          beforeEnter: requireAuth && isAdmin
        }
      ]
    },
    {
      path: '*',
      beforeEnter: checkIfPageExists
    }
  ]
})

export default router
