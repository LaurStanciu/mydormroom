import axios from 'axios'
import ErrorResponseService from '../services/ErrorResponseService'

let errorResponseService = new ErrorResponseService()

export default class RoomRequestService {
  getRoomRequests (id) {
    return new Promise((resolve, reject) => {
      axios.get('/api/roomrequests/findfullroomrequestbyidstudent/' + id)
        .then(response => {
          errorResponseService.resolveErrors(response)
          resolve(response)
        })
        .catch(error => {
          errorResponseService.resolveErrors(error)
          reject(error)
        })
    })
  }
  sendRoomRequest (roomRequest) {
    return new Promise((resolve, reject) => {
      console.log('service roomRequest')
      console.log(roomRequest)
      axios.post('/api/roomrequests/save', roomRequest)
        .then(response => {
          errorResponseService.resolveErrors(response)
          resolve(response)
        })
        .catch(error => {
          errorResponseService.resolveErrors(error)
          reject(error)
        })
    })
  }
  acceptRoomRequest (roomRequest) {
    return new Promise((resolve, reject) => {
      console.log('acceptRoomRequest')
      console.log(roomRequest)
      axios.post('/api/roomrequests/acceptroomrequest', roomRequest)
        .then(response => {
          errorResponseService.resolveErrors(response)
          resolve(response)
        })
        .catch(error => {
          errorResponseService.resolveErrors(error)
          reject(error)
        })
    })
  }
  removeRoomRequest (roomRequest) {
    return new Promise((resolve, reject) => {
      console.log('removeroomrequest')
      console.log(roomRequest)
      axios.post('/api/roomrequests/removeroomrequest', roomRequest)
        .then(response => {
          errorResponseService.resolveErrors(response)
          resolve(response)
        })
        .catch(error => {
          errorResponseService.resolveErrors(error)
          reject(error)
        })
    })
  }
}
