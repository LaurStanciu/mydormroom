import axios from 'axios'
import store from '../store'
import * as types from '../store/mutation-types'
import ErrorResponseService from '../services/ErrorResponseService'

let errorResponseService = new ErrorResponseService()

export default class CheckUpdatesService {
  checkUpdates () {
    return new Promise((resolve, reject) => {
      axios.get('/api/utility/checkupdates')
        .then(response => {
          if (response.status === 200) {
            store.commit(types.UPDATES, response.data)
          }
          errorResponseService.resolveErrors(response)
          // console.log('*In Check Token*')
          // console.log(response.headers)
          resolve(response)
          // console.log('*Out Check Token*')
        })
        .catch(error => {
          errorResponseService.resolveErrors(error)
          reject(error)
        })
    })
  }
}
