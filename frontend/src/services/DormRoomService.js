import axios from 'axios'
import ErrorResponseService from '../services/ErrorResponseService'

let errorResponseService = new ErrorResponseService()

export default class DormRoomService {
  getDorms () {
    return new Promise((resolve, reject) => {
      axios.get('/api/dorms/findall')
        .then(response => {
          errorResponseService.resolveErrors(response)
          resolve(response)
        })
        .catch(error => {
          errorResponseService.resolveErrors(error)
          reject(error)
        })
    })
  }
  getRooms (idDorm) {
    return new Promise((resolve, reject) => {
      axios.get('/api/rooms/findavailablebydorm/' + idDorm)
        .then(response => {
          errorResponseService.resolveErrors(response)
          resolve(response)
        })
        .catch(error => {
          errorResponseService.resolveErrors(error)
          reject(error)
        })
    })
  }
  getMyRoom (idStudent) {
    return new Promise((resolve, reject) => {
      axios.get('/api/rooms/myroom/' + idStudent)
        .then(response => {
          errorResponseService.resolveErrors(response)
          resolve(response)
        })
        .catch(error => {
          errorResponseService.resolveErrors(error)
          reject(error)
        })
    })
  }
}
