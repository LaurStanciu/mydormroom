import axios from 'axios'
import * as types from '../store/mutation-types'
import router from '../router'
import store from '../store'

axios.defaults.withCredentials = true

export default class LoginService {
  logInUser (username, password) {
    return new Promise((resolve, reject) => {
      axios.post('/api/login', { username, password })
        .then(response => {
          resolve(response)
        })
        .catch(error => {
          reject(error)
        })
    })
  }
  logOutUser () {
    store.commit(types.LOGOUT)
    router.push('/login')
  }
}
