import axios from 'axios'
import ErrorResponseService from '../services/ErrorResponseService'

let errorResponseService = new ErrorResponseService()

export default class ChangeRoomRequestService {
  getChangeRoomRequests (id) {
    return new Promise((resolve, reject) => {
      axios.get('/api/changeroomrequests/findbysource/' + id)
        .then(response => {
          errorResponseService.resolveErrors(response)
          resolve(response)
        })
        .catch(error => {
          errorResponseService.resolveErrors(error)
          reject(error)
        })
    })
  }
  createChangeRoomRequest (changeRoomRequest) {
    return new Promise((resolve, reject) => {
      console.log('createChangeRoomRequest')
      console.log(changeRoomRequest)
      axios.post('/api/changeroomrequests/create', changeRoomRequest)
        .then(response => {
          errorResponseService.resolveErrors(response)
          resolve(response)
        })
        .catch(error => {
          errorResponseService.resolveErrors(error)
          reject(error)
        })
    })
  }
  acceptChangeRoomRequest (changeRoomRequest) {
    return new Promise((resolve, reject) => {
      console.log('acceptChangeRoomRequest')
      console.log(changeRoomRequest)
      axios.post('/api/changeroomrequests/accept', changeRoomRequest)
        .then(response => {
          errorResponseService.resolveErrors(response)
          resolve(response)
        })
        .catch(error => {
          errorResponseService.resolveErrors(error)
          reject(error)
        })
    })
  }
  removeChangeRoomRequest (id) {
    return new Promise((resolve, reject) => {
      console.log('removeChangeRoomRequest')
      console.log(id)
      axios.get('/api/changeroomrequests/decline/' + id)
        .then(response => {
          errorResponseService.resolveErrors(response)
          resolve(response)
        })
        .catch(error => {
          errorResponseService.resolveErrors(error)
          reject(error)
        })
    })
  }
}
