import VueJwtDecode from 'vue-jwt-decode'

export default class JwtDecodeService {
  decode (token) {
    return VueJwtDecode.decode(token)
  }
}
