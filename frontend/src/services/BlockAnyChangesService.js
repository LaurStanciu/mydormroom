import axios from 'axios'
import ErrorResponseService from '../services/ErrorResponseService'

let errorResponseService = new ErrorResponseService()

export default class BlockAnyChangesService {
  getBlockAnyChanges () {
    return new Promise((resolve, reject) => {
      axios.get('/api/blockanychanges/findcurrentrestrictions')
        .then(response => {
          errorResponseService.resolveErrors(response)
          resolve(response)
        })
        .catch(error => {
          errorResponseService.resolveErrors(error)
          reject(error)
        })
    })
  }
  setBlockAnyChanges (blockAnyChanges) {
    return new Promise((resolve, reject) => {
      axios.post('/api/blockanychanges/save', blockAnyChanges)
        .then(response => {
          errorResponseService.resolveErrors(response)
          resolve(response)
        })
        .catch(error => {
          errorResponseService.resolveErrors(error)
          reject(error)
        })
    })
  }
  deleteBlockAnyChanges () {
    return new Promise((resolve, reject) => {
      axios.get('/api/blockanychanges/deleteall')
        .then(response => {
          errorResponseService.resolveErrors(response)
          resolve(response)
        })
        .catch(error => {
          errorResponseService.resolveErrors(error)
          reject(error)
        })
    })
  }
}
