import router from '../router'
import store from '../store'
import * as types from '../store/mutation-types'

export default class ErrorResponseService {
  resolveErrors (error) {
    if (error.data !== undefined) {
      if (error.status === 200) {
        store.commit(types.REFRESH_TOKEN, {
          token: error.headers.authorization
        })
      }
    } else if (error.response !== undefined) {
      if (error.response.data.status === 500) {
        if (error.response.data.exception.includes('ExpiredJwtException') ||
         error.response.data.exception.includes('SignatureException') ||
          error.response.data.exception.includes('MalformedJwtException')) {
          store.commit(types.LOGOUT)
          router.push('/login')
        }
      }
    }
  }
}
